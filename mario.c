#include <stdio.h>
#include <cs50.h>

int main(void)
{
    //Gets a positive integer between 1 and 8, inclusive
    int height = get_int("What is the height of your pyramid? Remember, it can only be between 1 and 8!\n");

    //Checks if the number is between 1 and 8, inclusive
    if (height < 1 || height > 8)
    {
        do
        {
            height = get_int("What is the height of your pyramid? Remember, it can only be between 1 and 8!\n");
        }
        while (height < 1 || height > 8);
    }
    int level = height;

    //Prints out the pyramids for Mario to jump over
    for (int i = height; i > 0; i--)
    {
        int gaps = height - 1;
        height--;

        //Prints out the spaces before the pyramid
        for (int space = gaps; space > 0; space--)
        {
            printf(" ");
        }

        //Prints first half of pyramid
        for (int j = 0; j < level - gaps; j++)
        {
            printf("#");
        }

        printf("  ");

        //Prints second half of pyramid
        for (int k = 0; k < level - gaps; k++)
        {
            printf("#");
        }

        printf("\n");

    }
}
