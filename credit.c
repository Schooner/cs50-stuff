#include <stdio.h>
#include <cs50.h>

int main(void)
{
    long cardNo = get_long("Number:"); //Gets the credit card number
    long rem = 0; //Stores the remainder
    long check = 0; //For checking validity of the cards
    long sum = 0; //Sum of digits which are not multiplied
    long product = 0; //Product of the digits
    long product_sum = 0; //Sum of the digits of the product

    //Gets input of card number until it is satisfactory
    while (cardNo < 0)
    {
        cardNo = get_long("Number:");
    }


    //Checks if it can possibly be a card number
    if (cardNo < 4000000000000)
    {
        printf("INVALID\n");
    }

    //Checks if it is an American Express card
    else if (cardNo >= 340000000000000 && cardNo <= 349999999999999)
    {
        //Finds the checksum value
        for (int i = 0; i < 15; i++)
        {
            if (i % 2 == 0)
            {
                sum = sum + (cardNo % 10);
            }

            else
            {

                product = (2 * (cardNo % 10));

                //Adds the products' digits together
                do
                {
                    if (product == 10)
                    {
                        product_sum = product_sum + 1;
                        product = 0;
                    }
                    else if (product < 10)
                    {
                        product_sum = product_sum + (product % 10);
                        product = 0;
                    }

                    else
                    {
                        product_sum = product_sum + (product % 10);
                        product = product - (product % 10);
                        product = product / 10;
                    }

                }
                while (product > 0);

            }

            if (i < 14)
            {
                rem = cardNo % 10;

                if (rem == 0)
                {
                    cardNo = cardNo / 10;
                }
                else
                {
                    cardNo = cardNo - rem;
                    cardNo = cardNo / 10;
                }
            }

            else
            {
                cardNo = cardNo % 10;
            }
        }

        check = (sum + product_sum) % 10;

        //Determines if the card is valid or not
        if (check == 0)
        {
            printf("AMEX\n");
        }
        else
        {
            printf("INVALID\n");
        }
    }

    //Checks if it is an American Express card
    else if (cardNo >= 370000000000000 && cardNo <= 379999999999999)
    {
        //Finds the checksum value
        for (int i = 0; i < 15; i++)
        {
            if (i % 2 == 0)
            {
                sum = sum + (cardNo % 10);
            }

            else
            {

                product = (2 * (cardNo % 10));

                //Adds the products' digits together
                do
                {
                    if (product == 10)
                    {
                        product_sum = product_sum + 1;
                        product = 0;
                    }
                    else if (product < 10)
                    {
                        product_sum = product_sum + (product % 10);
                        product = 0;
                    }

                    else
                    {
                        product_sum = product_sum + (product % 10);
                        product = product - (product % 10);
                        product = product / 10;
                    }

                }
                while (product > 0);

            }

            if (i < 14)
            {
                rem = cardNo % 10;

                if (rem == 0)
                {
                    cardNo = cardNo / 10;
                }
                else
                {
                    cardNo = cardNo - rem;
                    cardNo = cardNo / 10;
                }
            }

            else
            {
                cardNo = cardNo % 10;
            }

        }

        check = (sum + product_sum) % 10;

        //Determines if the card is valid or not
        if (check == 0)
        {
            printf("AMEX\n");
        }
        else
        {
            printf("INVALID\n");
        }
    }

    //Checks if it is a 13 digit Visa card
    else if (cardNo > 3999999999999 && cardNo < 4999999999999)
    {
        //Finds the checksum value
        for (int i = 0; i < 13; i++)
        {
            if (i % 2 == 0)
            {
                sum = sum + (cardNo % 10);
            }

            else
            {

                product = (2 * (cardNo % 10));

                //Adds the products' digits together
                do
                {
                    if (product == 10)
                    {
                        product_sum = product_sum + 1;
                        product = 0;
                    }
                    else if (product < 10)
                    {
                        product_sum = product_sum + (product % 10);
                        product = 0;
                    }

                    else
                    {
                        product_sum = product_sum + (product % 10);
                        product = product - (product % 10);
                        product = product / 10;
                    }

                }
                while (product > 0);

            }

            if (i < 12)
            {
                rem = cardNo % 10;

                if (rem == 0)
                {
                    cardNo = cardNo / 10;
                }
                else
                {
                    cardNo = cardNo - rem;
                    cardNo = cardNo / 10;
                }
            }

            else
            {
                cardNo = cardNo % 10;
            }

        }

        check = (sum + product_sum) % 10;

        //Determines if the card is valid or not
        if (check == 0)
        {
            printf("VISA\n");
        }
        else
        {
            printf("INVALID\n");
        }
    }

    //Checks if it is a 16 digit Visa card
    else if (cardNo > 3999999999999999 && cardNo < 4999999999999999)
    {
        for (int i = 0; i < 16; i++)
        {
            if (i % 2 == 0)
            {
                sum = sum + (cardNo % 10);

            }

            else
            {
                product = (2 * (cardNo % 10));

                //Adds the products' digits together
                do
                {
                    if (product == 10)
                    {
                        product_sum = product_sum + 1;
                        product = 0;
                    }
                    else if (product < 10)
                    {
                        product_sum = product_sum + (product % 10);
                        product = 0;
                    }

                    else
                    {
                        product_sum = product_sum + (product % 10);
                        product = product - (product % 10);
                        product = product / 10;
                    }

                }
                while (product > 0);

            }

            if (i < 15)
            {
                rem = cardNo % 10;

                if (rem == 0)
                {
                    cardNo = cardNo / 10;
                }
                else
                {
                    cardNo = cardNo - rem;
                    cardNo = cardNo / 10;
                }

            }

            else
            {
                cardNo = cardNo % 10;

            }

        }

        check = (sum + product_sum) % 10;



        //Determines if the card is valid or not
        if (check == 0)
        {
            printf("VISA\n");
        }
        else
        {
            printf("INVALID\n");
        }
    }

    //Checks if it is a Mastercard
    else if (cardNo > 5099999999999999 && cardNo < 5599999999999999)
    {
        //Finds the checksum value
        for (int i = 0; i < 16; i++)
        {
            if (i % 2 == 0)
            {
                sum = sum + (cardNo % 10);
                printf("sum at this point = %li\n", sum);
                printf("i at this point = %i\n", i);
            }

            else
            {

                product = (2 * (cardNo % 10));

                //Adds the products' digits together
                do
                {
                    if (product == 10)
                    {
                        product_sum = product_sum + 1;
                        product = 0;
                    }
                    else if (product < 10)
                    {
                        product_sum = product_sum + (product % 10);
                        product = 0;
                    }

                    else
                    {
                        product_sum = product_sum + (product % 10);
                        product = product - (product % 10);
                        product = product / 10;
                    }

                }
                while (product > 0);

            }

            if (i < 15)
            {
                rem = cardNo % 10;

                if (rem == 0)
                {
                    cardNo = cardNo / 10;
                }
                else
                {
                    cardNo = cardNo - rem;
                    cardNo = cardNo / 10;
                }
            }

            else
            {
                cardNo = cardNo % 10;
            }
            printf("Card no. at this point = %li\n", cardNo);

        }

        check = (sum + product_sum) % 10;

        //Determines if the card is valid or not
        if (check == 0)
        {
            printf("MASTERCARD\n");
        }
        else
        {
            printf("INVALID\n");
        }
    }

    //Checks if it is a 16 digit Visa card
    else if (cardNo > 3999999999999999 && cardNo < 4999999999999999)
    {
        //Finds the checksum value
        for (int i = 0; i < 16; i++)
        {
            if (i % 2 == 0)
            {
                sum = sum + (cardNo % 10);


            }

            else
            {
                product = (2 * (cardNo % 10));

                //Adds the products' digits together
                do
                {
                    if (product == 10)
                    {
                        product_sum = product_sum + 1;
                        product = 0;
                    }
                    else if (product < 10)
                    {
                        product_sum = product_sum + (product % 10);
                        product = 0;
                    }

                    else
                    {
                        product_sum = product_sum + (product % 10);
                        product = product - (product % 10);
                        product = product / 10;
                    }


                }
                while (product > 0);

            }

            if (i < 15)
            {
                rem = cardNo % 10;

                if (rem == 0)
                {
                    cardNo = cardNo / 10;
                }
                else
                {
                    cardNo = cardNo - rem;
                    cardNo = cardNo / 10;
                }

            }

            else
            {
                cardNo = cardNo % 10;

            }

        }

        check = (sum + product_sum) % 10;



        //Determines if the card is valid or not
        if (check == 0)
        {
            printf("VISA\n");
        }
        else
        {
            printf("INVALID\n");
        }

    }

    //Declares invalidity after checking every option
    else
    {
        printf("INVALID\n");
    }

}