#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

int main(int argc, string argv[])
{
    int count = 0;
    if (argc != 2)//Checks if the user has entered a key
    {
        printf("Usage: ./substitution KEY\n");
        return 1;
    }
    else if (argc == 2)//Checks if the entered key is valid
    {
        if (strlen(argv[1]) == 26)
        {
            for (int i = 0; i < 26; i++)//Checks if the key has 26 characters
            {
                if (isalpha(argv[1][i]))//Checks if the character is an alphabet
                {
                    for (int j = 0; j < 26; j++)//Checks if a character is repeated
                    {
                        if (argv[1][i] == argv[1][j])
                        {
                            count++;
                            if (count > 1)//Error message if the key contains duplicate characters
                            {
                                printf("Key cannot contain duplicate characters\n");
                                return 1;
                            }
                        }
                    }
                    count = 0;
                }
                else//Error message if the key contains non-alphabetic characters
                {
                    printf("Key must only contain alphabetic characters\n");
                    return 1;
                }
            }
        }
        else//Error message if the key does not contain 26 characters
        {
            printf("Key must contain 26 characters\n");
            return 1;
        }
    }

    //printf("Gets here\n");
    string plain = get_string("plaintext: ");
    printf("ciphertext: ");
    int pos = 0;
    //printf("Gets here\n");

    for (int i = 0, n = strlen(plain); i < n; i++)//Substitutes it for ciphertext
    {
        if (isupper(plain[i]))//Checks if character is uppercase
        {
            pos = (int) plain[i] - 65;
            printf("%c", toupper(argv[1][pos]));
        }
        else if (islower(plain[i])) //Checks if character is lowercase
        {
            pos = (int) plain[i] - 97;
            printf("%c", tolower(argv[1][pos]));
        }
        else//Prints character if not an alphabet
        {
            printf("%c", plain[i]);
        }
    }
    printf("\n");
}