#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

int main(void)
{
    string s = get_string("Text: ");
    int letters = 0, words = 0, sentences = 0;

    //Calculates the number of letters, words and sentences in the text
    for (int i = 0, length = strlen(s); i < length; i++)
    {
        //Checks if it's a sentence
        if ((s[i] == '?' || s[i] == '!') || s[i] == '.')
        {
            sentences++;
        }
        //Checks if it's a letter
        else if (isalpha(s[i]) != 0)
        {
            letters++;

            //Checks if it's a word
            if (words == 0)//Incrementing on first encountering a letter
            {
                words++;
            }
            else if (isalpha(s[i]) != 0 && (s[i - 1] == ' ' || s[i - 1] == '"'))//Checking preceding characters to see if it is a word
            {
                words++;
            }
        }
    }

    float L = letters * (100 / (float) words);
    float S = sentences * (100 / (float) words);
    float index = 0.0588 * L - 0.296 * S - 15.8;

    if (index < 1)
    {
        printf("Before Grade 1\n");
    }
    else if (index >= 1 && index <= 16)
    {
        printf("Grade %i\n", (int) round(index));
    }
    else if (index > 16)
    {
        printf("Grade 16+\n");
    }
}